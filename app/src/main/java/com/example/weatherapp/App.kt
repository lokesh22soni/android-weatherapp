package com.example.weatherapp

import android.app.Activity
import android.app.Application
import android.util.Log
import com.example.weatherapp.di.component.AppComponent
import com.example.weatherapp.di.component.DaggerAppComponent
import com.example.weatherapp.di.module.AppModule
import com.example.weatherapp.di.module.NetModule
import com.example.weatherapp.di.module.RetrofitModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

open class App: Application(),HasAndroidInjector {
    val TAG = App::class.java.name

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    companion object {
        private lateinit var instance: App
        @Synchronized
        fun getInstance(): App = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        getComponent().inject(this)
    }
    private var applicationComponent: AppComponent? = null

    private fun getComponent(): AppComponent {

        Log.i("application", "App comp")
        if (applicationComponent == null) {
            applicationComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this)).netModule(NetModule()).retrofitModule(RetrofitModule())
                .build()
        }
        return applicationComponent!!
    }

    override fun androidInjector(): AndroidInjector<Any> = activityInjector as AndroidInjector<Any>
}