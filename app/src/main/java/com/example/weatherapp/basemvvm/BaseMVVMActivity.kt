package com.example.weatherapp.basemvvm

import android.app.ProgressDialog
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.Gravity
import android.view.KeyEvent
import android.widget.FrameLayout
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.weatherapp.R
import com.google.android.material.snackbar.Snackbar
import com.example.weatherapp.utils.Constants

open class BaseMVVMActivity : AppCompatActivity() {
    lateinit var permissionListener:PermissionRequestListener
    private val TAG = BaseMVVMActivity::class.java.simpleName
    lateinit var netmonitor: NetStatusReceiver
    lateinit var netIntent: IntentFilter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_mvvm)
        instanceObj = this
        netmonitor = NetStatusReceiver()
        netIntent = IntentFilter()
        netIntent.addAction("android.net.conn.CONNECTIVITY_CHANGE")
    }
    fun localFunction(){
        Toast.makeText(this,"Toast from base activity",Toast.LENGTH_SHORT).show()
    }
    companion object{
        var instanceObj:BaseMVVMActivity?=null
    }
    abstract interface PermissionRequestListener {
        fun permissionResponse(permissions: Array<out String>, grantResults: IntArray)
    }
    fun checkPermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            permission
        ) != PackageManager.PERMISSION_GRANTED
    }

    fun checkPermissions(permissions: Array<String>): Boolean {
        for (permission in permissions) {
            if (checkPermission(permission)) {
                return true
            }
        }
        return false
    }
    fun requestPermission(permission: Array<String>) {
        ActivityCompat.requestPermissions(this, permission, Constants.PERMISSION_REQUSET_CODE)
    }
    override fun onPause() {
        super.onPause()
        // unregistering the network monitor
        unregisterReceiver(netmonitor)
    }
    override fun onResume() {
        super.onResume()
        registerReceiver(netmonitor, netIntent)
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constants.PERMISSION_REQUSET_CODE) {
            permissionListener.permissionResponse(permissions, grantResults)
        }
    }

    var isConnected: Boolean = false
    var connectionStatus: Int = 0
    private var progressDialog: ProgressDialog? = null
    // Snack bar on top to notice internet availability
    open fun showConnectionStatus(status: Boolean) {
        val msg =
            if (status) getString(R.string.net_available) else getString(R.string.net_not_available)
        val snack = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG)
        val view = snack.view
        val params = view.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.TOP
        view.layoutParams = params
        if (status)
            view.setBackgroundColor(Color.GREEN)
        else
            view.setBackgroundColor(Color.RED)
        snack.show()
    }
    /**
     * Broadcast receiver to monitor network availability
     */
    class NetStatusReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val conectivity =
                context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = conectivity.activeNetworkInfo
            (context as BaseMVVMActivity).isConnected = activeNetwork?.isConnected ?:false
            if (!(context as BaseMVVMActivity).isConnected)
                (context as BaseMVVMActivity).showConnectionStatus((context as BaseMVVMActivity).isConnected)
            if(activeNetwork != null) {
                if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                    // do something
                } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                    // check NetworkInfo subtype
                    if (activeNetwork.subtype == TelephonyManager.NETWORK_TYPE_GPRS) {
                        // Bandwidth between 100 kbps and below
                        (context as BaseMVVMActivity).connectionStatus =
                            activeNetwork.subtype//"Connection Moderate"

                    } else if (activeNetwork.subtype == TelephonyManager.NETWORK_TYPE_EDGE) {
                        // Bandwidth between 50-100 kbps
                        (context as BaseMVVMActivity).connectionStatus =
                            activeNetwork.subtype // "Connection Poor"
                    } else if (activeNetwork.subtype == TelephonyManager.NETWORK_TYPE_EVDO_0) {
                        // Bandwidth between 400-1000 kbps
                        (context as BaseMVVMActivity).connectionStatus =
                            activeNetwork.subtype // "Connection Good"
                    } else if (activeNetwork.subtype == TelephonyManager.NETWORK_TYPE_EVDO_A) {
                        // Bandwidth between 600-1400 kbps
                        (context as BaseMVVMActivity).connectionStatus =
                            activeNetwork.subtype // "Connection Good"
                    }

                    // Other list of various subtypes you can check for and their bandwidth limits
                    // TelephonyManager.NETWORK_TYPE_1xRTT       ~ 50-100 kbps
                    // TelephonyManager.NETWORK_TYPE_CDMA        ~ 14-64 kbps
                    // TelephonyManager.NETWORK_TYPE_HSDPA       ~ 2-14 Mbps
                    // TelephonyManager.NETWORK_TYPE_HSPA        ~ 700-1700 kbps
                    // TelephonyManager.NETWORK_TYPE_HSUPA       ~ 1-23 Mbps
                    // TelephonyManager.NETWORK_TYPE_UMTS        ~ 400-7000 kbps
                    // TelephonyManager.NETWORK_TYPE_UNKNOWN     ~ Unknown

                }
            }
        }

    }
    fun showProgressDialog(@StringRes messageId: Int) {
        if (progressDialog == null) {
            progressDialog = ProgressDialog(this)
            progressDialog!!.isIndeterminate = true
            progressDialog!!.setCancelable(false)
            progressDialog!!.setCanceledOnTouchOutside(false)

            // Disable the back button
            val keyListener = DialogInterface.OnKeyListener { dialog,
                                                              keyCode,
                                                              event ->
                keyCode == KeyEvent.KEYCODE_BACK
            }
            progressDialog!!.setOnKeyListener(keyListener)
        }
        progressDialog!!.setMessage(getString(messageId))
        progressDialog!!.show()
    }

    fun hideProgressDialog() {
        progressDialog?.let {
            if (it.isShowing) {
                it.dismiss()
            }
        }
    }

    fun isProgresDialogShowing(): Boolean {
        if (progressDialog != null && progressDialog?.isShowing != null) {
            return progressDialog!!.isShowing
        } else {
            return false
        }
    }
}