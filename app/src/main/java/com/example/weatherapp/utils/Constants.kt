package com.example.weatherapp.utils

import android.Manifest
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.widget.Toast
import com.example.weatherapp.BuildConfig

/**
 * Constants for app/intent keys
 */
class Constants {
    companion object {
        const val PERMISSION_REQUSET_CODE = 200
    }
}

/**
 * Constants for network api keys
 */
class ApiConstants {
    companion object {
        const val NOAUTH = "no_auth"

        /*
    * Open Application settings page to set the permissions or data access*/
        fun openSettings(context: Context) {
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri = Uri.fromParts(
                "package",
                BuildConfig.APPLICATION_ID, null
            )
            intent.data = uri
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }

        fun showToastMessage(context: Context, msg: String) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        }

    }
}

