package com.example.weatherapp.utils

enum class StatusMode {
    SUCCESS,
    ERROR,
    LOADING
}