package com.example.weatherapp.ui

import com.example.weatherapp.data.model.WeatherResponse

/**
 * Data results for Data response Api call.
 */
data class MainClassResult(
    var responseData: WeatherResponse? = null,
    val errorCode: Int? = null,
    val error: String? = null
)

