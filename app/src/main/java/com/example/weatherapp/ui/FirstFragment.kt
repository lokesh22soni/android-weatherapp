package com.example.weatherapp.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.weatherapp.BuildConfig.WEATHER_APP_ID
import com.example.weatherapp.R
import com.example.weatherapp.utils.ApiConstants
import com.example.weatherapp.utils.GPSTracker
import kotlinx.android.synthetic.main.fragment_first.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        displayDate.text = "Change Date: " + getCurrentDate()

        submitDate.setOnClickListener {
            displayDate.text = "Change Date: " + getCurrentDate()
            val datecheck =
                datePicker.dayOfMonth.toString() + (datePicker.month + 1).toString() + datePicker.year.toString()
            if (checkPrime(datePicker.dayOfMonth)) {
                //selected date is prime -> to be proceed
                //Retrofit call URL to get Weather data.
                (activity as MainActivity).showProgressDialog(R.string.pogress_dlg)
                (activity as MainActivity).mainClassViewModel.weatherDataApi(
                    GPSTracker.latitude.toString(), GPSTracker.longitude.toString(),
                    WEATHER_APP_ID,
                    (datePicker.month + 1),
                    datePicker.dayOfMonth

                )
            } else {
                activity?.let { it1 ->
                    ApiConstants.showToastMessage(
                        it1,
                        getString(R.string.date_not_prime)
                    )
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        /*
        * removing observer if already attached*/
        if ((requireActivity() as MainActivity).mainClassViewModel.weatherDataResult.hasObservers()) {
            (requireActivity() as MainActivity).mainClassViewModel.weatherDataResult.removeObserver {
                it.responseData = null
            }
        }
        (requireActivity() as MainActivity).mainClassViewModel.weatherDataResult.observe(
            requireActivity(),
            Observer {
                val resultData = it ?: return@Observer
                (activity as MainActivity).hideProgressDialog()
                when {
                    resultData.error != null -> {
                        ApiConstants.showToastMessage(requireActivity(), resultData.error)
                    }
                    resultData.errorCode != null -> {
                        ApiConstants.showToastMessage(
                            requireActivity(),
                            resources.getString(R.string.unknown_error)
                        )
                    }
                    resultData.responseData != null -> {
                        val weatherResponse = resultData.responseData!!
                        Log.i("WeatherResponse:", weatherResponse.toString())
                        val stringBuilder = """
                                Country: ${weatherResponse.sys!!.country}
                                Temperature: ${weatherResponse.main!!.temp}
                                Temperature(Min): ${weatherResponse.main!!.temp_min}
                                Temperature(Max): ${weatherResponse.main!!.temp_max}
                                Humidity: ${weatherResponse.main!!.humidity}
                                Pressure: ${weatherResponse.main!!.pressure}
                                WeatherPoint: ${weatherResponse.weather?.get(0)?.description!!}
                                """.trimIndent()
                        //                    weatherData.setText(stringBuilder)
                        ApiConstants.showToastMessage(requireActivity(), stringBuilder)

                        val bundleData = Bundle()
                        bundleData.putString("Country", weatherResponse.sys!!.country)
                        val tempStr = convertKelvinToDegreeCelsius(weatherResponse.main!!.temp)

                        bundleData.putString("Temperature", tempStr)
                        val tempMin = convertKelvinToDegreeCelsius(weatherResponse.main!!.temp_min)
                        bundleData.putString(
                            "TemperatureMin",
                            tempMin
                        )
                        val tempMax = convertKelvinToDegreeCelsius(weatherResponse.main!!.temp_max)
                        bundleData.putString(
                            "TemperatureMax",
                            tempMax
                        )
                        bundleData.putString("Humidity", weatherResponse.main!!.humidity.toString())
                        bundleData.putString("Pressure", weatherResponse.main!!.pressure.toString())
                        bundleData.putString(
                            "WeatherPoint",
                            weatherResponse.weather?.get(0)?.description!!
                        )

                        resultData.responseData = null

                        findNavController().navigate(
                            R.id.action_FirstFragment_to_SecondFragment,
                            bundleData
                        )
                    }
                }

            })
    }

    private fun convertToDegreeCelsius(value: Float): String {
        val a = value.toDouble()
        val b: Double = a - 32
        val c: Double = b * 5 / 9
        val d = String.format("%.2f", c)
        return "$d°C"
    }

    private fun convertKelvinToDegreeCelsius(value: Float): String {
        val a = value.toDouble()
        val b: Double = a - 273.15
        val c = String.format("%.2f", b)
        return "$c°C"
    }

    private fun getCurrentDate(): String? {
        val builder = StringBuilder()
        builder.append((datePicker.month + 1).toString() + "/") //month is 0 based
        builder.append(datePicker.dayOfMonth.toString() + "/")
        builder.append(datePicker.year)
        return builder.toString()
    }

    // a number is not prime if there is a prime number between 2 to the square root of
    // num that evenly divides it
    private fun checkPrime(num: Int): Boolean {
        // get the prime numbers from 2 to square root of num.
        var i: Int

        var flag = 0

        val m = num / 2
        if (num == 0 || num == 1) {
            println("$num is not prime number")
            return false
        } else {
            i = 2
            while (i <= m) {
                if (num % i == 0) {
                    println("$num is not prime number")
                    flag = 1
                    break
                }
                i++
            }
            if (flag == 0) {
                println("$num is prime number")
                return true
            }
            return false
        } //end of else

    }
}