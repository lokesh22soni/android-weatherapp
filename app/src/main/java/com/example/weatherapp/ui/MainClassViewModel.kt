package com.example.weatherapp.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.weatherapp.R
import com.example.weatherapp.data.repository.MainClassRepository
import com.example.weatherapp.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import com.example.weatherapp.data.repository.*
import javax.inject.Inject

/**
 * Created by Lokesh Soni on 23/12/2020.
 */
class MainClassViewModel @Inject constructor(private val mainClassRepository: MainClassRepository) :
    BaseViewModel() {

    /**
     * livedata to observe Weather api result data
     */
    val _weatherDataResult = MutableLiveData<MainClassResult>()
    val weatherDataResult: LiveData<MainClassResult> = _weatherDataResult
    fun weatherDataApi(
        lat: String,
        lon: String,
        app_id: String,
        month: Int,
        day: Int
    ) {
        // can be launched in a separate asynchronous job
        launch {
            val result = mainClassRepository.weatherApiCall(lat, lon, app_id, month, day)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success -> {
                        val responseData = result.data
                        _weatherDataResult.value =
                            MainClassResult(responseData = responseData)

                    }
                    is Result.Error -> {
                        _weatherDataResult.value =
                            MainClassResult(error = result.msg, errorCode = result.resId)
                    }
                    else -> _weatherDataResult.value =
                        MainClassResult(errorCode = R.string.unknown_error)
                }
            }
        }
    }
}