package com.example.weatherapp.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

/**
 * Created by Lokesh Soni on 24/12/2020.
 */
class MainClassViewModelFactory @Inject constructor(private val mainClassViewModel: MainClassViewModel):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainClassViewModel::class.java)) {
            return mainClassViewModel as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}