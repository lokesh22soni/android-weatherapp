package com.example.weatherapp.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.example.weatherapp.R
import kotlinx.android.synthetic.main.fragment_second.*

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if(arguments!=null){
            val country = arguments?.getString("Country")
            arguments?.getString("Country")?.let { Log.i("CountryTag", it) }
            val temperature = arguments?.getString("Temperature")
            val temperatureMin = arguments?.getString("TemperatureMin")
            val temperatureMax = arguments?.getString("TemperatureMax")
            val humidity = arguments?.getString("Humidity")
            val pressure = arguments?.getString("Pressure")
            val weatherPoint = arguments?.getString("WeatherPoint")

            val weatherDetails = "Country $country\n\nTemperature $temperature\n\nTemperature(Min) $temperatureMin\n\nTemperature(Max) $temperatureMax\n\nHumidity $humidity\n\nPressure $pressure\n\nWeather $weatherPoint"
            weatherTempDetails?.text = weatherDetails

        }
        view?.findViewById<Button>(R.id.button_second)?.setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }
    }
}