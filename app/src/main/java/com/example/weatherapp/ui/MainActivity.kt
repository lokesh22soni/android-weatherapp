package com.example.weatherapp.ui

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.AndroidException
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.weatherapp.R
import com.example.weatherapp.basemvvm.BaseMVVMActivity
import com.example.weatherapp.basemvvm.BaseMVVMActivity.PermissionRequestListener
import com.example.weatherapp.utils.ApiConstants.Companion.openSettings
import com.example.weatherapp.utils.GPSTracker
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import javax.inject.Inject

class MainActivity : BaseMVVMActivity(), PermissionRequestListener {

    @Inject
    lateinit var mainClassViewModelFactory: MainClassViewModelFactory
    lateinit var mainClassViewModel: MainClassViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AndroidInjection.inject(this)
//        setSupportActionBar(findViewById(R.id.toolbar))
//      initialize viewModelClass
        instanceObj?.localFunction()
        mainClassViewModel = ViewModelProviders.of(this, mainClassViewModelFactory)
            .get(MainClassViewModel::class.java)

//        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show()
//        }

        checkPermission()
    }

    private var permissions: ArrayList<String>? = null
    var allPermissionGranted = false

    /*
    * call for Check Users permission
    *location access*/
    private fun checkPermission() {
        permissionListener = this
        permissions = arrayListOf()
        if (checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            permissions?.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }

        if (permissions?.isNotEmpty()!!) {
            requestPermission(permissions?.toTypedArray()!!)
        } else {
            val intentService = Intent(this@MainActivity, GPSTracker::class.java)
            startService(intentService)
        }
    }

    /*
    * call for Permissions Response*/
    override fun permissionResponse(permissions: Array<out String>, grantResults: IntArray) {
        for (i in grantResults.indices) {
            allPermissionGranted = grantResults[i] == PackageManager.PERMISSION_GRANTED
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                // user rejected the permission
                val showRationale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    shouldShowRequestPermissionRationale(permissions[i])
                } else {
                    false
                }
                if (!showRationale) {
                    //execute when 'never Ask Again' tick and permission dialog not show
                    val message =
                        if (permissions[i] == Manifest.permission.ACCESS_FINE_LOCATION)
                            resources.getString(R.string.location_permission_msg).toString()
                        else {
                            resources.getString(R.string.location_permission_not_granted)
                        }
                    showMessageOKCancel(message,
                        DialogInterface.OnClickListener { dialog, which ->
                            openSettings(this)
                            dialog?.dismiss()
                        })
                    openSettings(this)
                } else {

                    requestPermission(this.permissions?.toTypedArray()!!)
                }
                allPermissionGranted = false
                break
            }
        }

        /*
        * call for AppFile class initialization
        * (Create the File Storage path and make directory to save the temporary files*/
        if (allPermissionGranted) {
            //All permissions granted
            val intentService = Intent(this@MainActivity, GPSTracker::class.java)
            startService(intentService)
        }
    }

    /*
        * call for showing message dialog*/
    private fun showMessageOKCancel(
        message: String,
        okListener: DialogInterface.OnClickListener
    ) {
        androidx.appcompat.app.AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton(resources.getString(R.string.action_ok), okListener)
            .setNegativeButton(resources.getString(R.string.cancel_txt), null)
            .setCancelable(false)
            .create()
            .show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}