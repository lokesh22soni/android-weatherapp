package com.example.weatherapp.di.component

import com.example.weatherapp.App
import com.example.weatherapp.di.module.*
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * class to define the components that are provided for the app with the defined list of modules
 */
@Singleton
@Component(
    modules = [(AndroidInjectionModule::class),(AppModule::class),(NetModule::class),(RetrofitModule::class),(ActivityBuldersModule::class)]
)
interface AppComponent {
    fun inject(app: App)
}