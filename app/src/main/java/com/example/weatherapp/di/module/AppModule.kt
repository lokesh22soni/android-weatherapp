package com.example.weatherapp.di.module

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * class to provide injection for application level access
 */
@Module
class AppModule (val app:Application){
    @Provides
    @Singleton
    fun provideApplication(): Application = app

}