package com.example.weatherapp.di.module

import com.example.weatherapp.basemvvm.BaseMVVMActivity
import com.example.weatherapp.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * class to provide the injection for the activities
 */
@Module
abstract class ActivityBuldersModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeBaseMVVMActivity(): BaseMVVMActivity
}