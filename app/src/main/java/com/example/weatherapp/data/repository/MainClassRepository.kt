package com.example.weatherapp.data.repository

import android.util.Log
import com.example.weatherapp.R
import com.example.weatherapp.data.apis.ApiInterface
import com.example.weatherapp.data.model.BaseResponse
import com.example.weatherapp.data.model.WeatherResponse
import com.squareup.moshi.Moshi
import org.json.JSONObject
import javax.inject.Inject

/**
 * Created by Lokesh Soni on 23/12/2020.
 */
class MainClassRepository @Inject constructor(val apiInterface: ApiInterface) {
    @Inject
    lateinit var moshi: Moshi

    /**
     * function for weather data open api call
     */
    fun weatherApiCall(
        lat: String,
        lon: String,
        api_id: String,
        month: Int,
        day: Int
    ): Result<WeatherResponse>? {

        try {
            // TODO: handle loggedInUser authentication
            val response =
                apiInterface.getCurrentWeatherData(
                    lat,
                    lon,
                    api_id,
                    month,
                    day
                )?.execute()
            Log.i("loggedInUser ", response.toString())
            return when {
                response?.body() is WeatherResponse -> {
                    val userData = response.body()
                    print("success")
                    Result.Success(userData!!)
                }
                response?.errorBody() != null -> {
                    val error = response.errorBody()?.string()
                    if (error?.contains("messages")!!) {
                        val adapter = moshi.adapter(BaseResponse::class.java).lenient()
                        val errorAdapt = adapter.fromJson(error)
                        Result.Error(
                            msg = errorAdapt?.errors?.messages?.get(0),
                            statusCode = response.code()
                        )
                    } else {
                        val errorJson = JSONObject(error)
                        Result.Error(
                            msg = errorJson.getString("errors"),
                            statusCode = response.code()
                        )
                    }
                }
                else -> {
                    print("Error unknown")
                    Result.Error(resId = R.string.unknown_error)
                }
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            return Result.Error(resId = R.string.unknown_error)
        }
    }
}