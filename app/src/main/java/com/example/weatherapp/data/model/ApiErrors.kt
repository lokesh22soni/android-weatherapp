package com.example.weatherapp.data.model


import com.squareup.moshi.Json

data class ApiErrors(
    @Json(name = "messages")
    val messages: List<String>
)