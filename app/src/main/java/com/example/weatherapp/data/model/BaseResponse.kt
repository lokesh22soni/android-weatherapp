package com.example.weatherapp.data.model

import com.squareup.moshi.Json

open class BaseResponse (
    @Json(name = "errors")
    var errors: ApiErrors? = null
)