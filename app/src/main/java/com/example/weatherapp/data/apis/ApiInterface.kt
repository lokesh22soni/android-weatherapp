package com.example.weatherapp.data.apis

import com.example.weatherapp.data.model.WeatherResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    @GET("data/2.5/weather?")
    fun getCurrentWeatherData(
        @Query("lat") lat: String?,
        @Query("lon") lon: String?,
        @Query("APPID") app_id: String?,
        @Query("month") month: Int,
        @Query("day") day: Int
    ): Call<WeatherResponse?>?
}